const canvasSketch = require('canvas-sketch');
// canvas-sketch fonctionne un peu comme processing on précise nos affaires et 
const settings = {
  // plusieurs dimensions de dispo A4, letter, ...
  // le pixel per inch est le même que pour photoshop
  dimensions: [ 2048, 2048 ]
  // dimensions: 'A4',
  // units permet d'utiliser des centimètre ou pouces ou autres. Très pratique
  // units: 'cm',
  // orientation landscape ou portrait
  // orientation: 'portrait',
  // pixelsPerInch: 300 
};

const sketch = () => {
  /* Function disposant du context du canvas la hauteur et largeur*/
  return ({ context, width, height }) => {
    context.fillStyle = 'transparent';
    context.fillRect(0, 0, width, height);
    context.beginPath();
    context.arc( width / 2, height / 2, width * 0.2, 0, Math.PI * 2, false);
    context.fillStyle = 'orange';
    context.fill();
    // contour de ligne si on le fait en se basant sur le width ou la height o
    context.lineWidth = width * 0.05;
    context.strokeStyle = 'salmon';
    context.stroke();
  };
};

canvasSketch(sketch, settings);
