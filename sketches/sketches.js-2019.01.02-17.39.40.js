const canvasSketch = require('canvas-sketch');

const settings = {
  dimensions: [ 2048, 2048 ]
};

const sketch = () => {
  const createGrid = () => {
    const points = [];
    const counts = 5;
    for (let x = 0; x < counts; x++) {
      for (let y = 0; y < counts; y++) {
        const u = counts <= 1 ? 0.5 : x / (counts - 1);
        const v = counts <= 1 ? 0.5 : y / (counts - 1 );
        points.push([u, v]);
      }
    }
    return points;
  };
  const points = createGrid();
  return ({ context, width, height }) => {
    context.fillStyle = 'white';
    context.fillRect(0, 0, width, height);
    points.forEach(([u, v]) => {
      const x = u * width;
      const y = v * height;

      context.beginPath();
      context.arc(x, y, 100, 0, Math.PI * 2, false);
      context.strokeStyle = 'black';
      context.lineWidth = 10;
      context.stroke();
    });
  };
};

canvasSketch(sketch, settings);
