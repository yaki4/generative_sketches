const canvasSketch = require('canvas-sketch');
//lerp linear interpolation (min, max, t) ou t est entre 0 et 1
const { lerp } = require('canvas-sketch-util/math');
const random = require('canvas-sketch-util/random');
const palettes = require('nice-color-palettes');
const settings = {
  suffix: random.getSeed(),
  dimensions: [ 2048, 2048 ]
};
const shapes = ['circles', 'rectangles', 'triangles', 'char']
random.setSeed(random.getRandomSeed());
console.log(random.getSeed());
const sketch = () => {
  const randomColor = random.rangeFloor(2, 4);
  const palette = random.shuffle(random.pick(palettes)).slice(0, randomColor);
  const createGrid = () => {
    const points = [];
    const counts = 40;
    for (let x = 0; x < counts; x++) {
      for (let y = 0; y < counts; y++) {
        const u = counts <= 1 ? 0.5 : x / (counts - 1);
        const v = counts <= 1 ? 0.5 : y / (counts - 1 );
        const radius = Math.abs(random.noise2D(u, v)) * 0.025;
        points.push({
          // radius:Math.abs(0.01 + random.gaussian() * 0.01),
          radius,
          color: random.pick(palette),
          position: [u, v],
          type: random.pick(shapes),
          rotation: random.noise2D(u, v)
        });
      }
    }
    return points;
  };
  //random.setSeed('bonjour');
  const points = createGrid().filter(() => random.value() > 0.5);
  const margin = 400;
  return ({ context, width, height }) => {
    context.fillStyle = 'transparent';
    context.fillRect(0, 0, width, height);
    // deconstruire l'object avec le premier const
    points.forEach(data => {
      const {
        position,
        radius,
        color,
        rotation,
        type
      } = data;
      const [u, v] = position;
      /* const x =lerp(margin, width - margin, u) * random.value();
      const y = lerp(margin, height - margin, v) * random.value(); */
      const x =lerp(margin, width - margin, u);
      const y = lerp(margin, height - margin, v);
      /* context.beginPath();
      context.arc(x, y, radius * width, 0, Math.PI * 2, false);
      context.fillStyle = color;
      context.fill(); */
      context.save();
      if (type === 'char') {
        context.fillStyle = color;
        context.font = `${(radius * (random.value() + 6)) * width}px "Helvetical"`;
        context.translate(x, y),
        context.rotate(rotation);
        context.fillText('-', 0, 0);
      } else if (type == 'circles') {
        context.beginPath();
        context.arc(x, y, radius * width, 0, Math.PI * 2, false);
        context.translate(x, y),
        context.rotate(rotation);
        context.fillStyle = color;
        context.fill();
      } else if (type === 'rectangles') {
        context.beginPath();
        context.rect(x, y, radius * width, radius * height);
        context.translate(x, y),
        context.rotate(rotation);
        context.fillStyle = color;
        context.fill();
      } else if (type === 'triangles') {
        context.beginPath();
        context.rotate(rotation);
        context.moveTo(x, y);
        context.lineTo(x + (10 + random.value()),x);
        context.lineTo(x + (5 + random.value()), y - (10 + random.value()));
        context.fillStyle = color;
        context.fill();
      }

      context.restore();
    });
  };
};

canvasSketch(sketch, settings);
